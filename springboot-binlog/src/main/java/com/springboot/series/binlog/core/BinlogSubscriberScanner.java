package com.springboot.series.binlog.core;

import com.lingshi.binlog.core.BinlogSubscriber;
import com.lingshi.binlog.data.BinlogType;
import com.springboot.series.binlog.core.anno.BSubscribe;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author shanlingshi
 * @since 2022-08-02
 */
@Slf4j
@Component
public class BinlogSubscriberScanner implements BeanPostProcessor {

    private static Map<BinlogType, Map<String, BinlogSubscriber>> BINLOG_SUBSCRIBERS = new ConcurrentHashMap<>();

    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        Class<?> beanClass = bean.getClass();

        for (Method method : beanClass.getMethods()) {
            BSubscribe annotation = method.getAnnotation(BSubscribe.class);
            if (annotation != null) {
                BINLOG_SUBSCRIBERS.computeIfAbsent(annotation.type(), key -> new HashMap<>()).put(annotation.table(),
                        data -> {
                            try {
                                method.invoke(bean, data);
                            } catch (IllegalAccessException | InvocationTargetException e) {
                                throw new RuntimeException(e);
                            }
                        });
            }
        }

        return bean;
    }

    public Map<BinlogType, Map<String, BinlogSubscriber>> getBinlogSubscribers() {
        return BINLOG_SUBSCRIBERS;
    }

}
