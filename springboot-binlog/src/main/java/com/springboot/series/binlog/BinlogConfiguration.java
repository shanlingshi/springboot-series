package com.springboot.series.binlog;

import com.lingshi.binlog.core.BinlogDataClient;
import com.lingshi.binlog.props.BinlogDataProps;
import com.mysql.cj.conf.ConnectionUrlParser;
import com.mysql.cj.conf.HostInfo;
import com.springboot.series.binlog.core.BinlogSubscriberScanner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import javax.annotation.Resource;

/**
 * @author shanlingshi
 */
@Configuration
@ConditionalOnClass(BinlogSubscriberScanner.class)
public class BinlogConfiguration {

    @Resource
    private Environment environment;
    @Resource
    private BinlogSubscriberScanner subscriberScanner;

    @Bean
    public BinlogDataProps binlogDataProps() {
        BinlogDataProps binlogDataProps = new BinlogDataProps();
        binlogDataProps.setDatasourceId(Integer.parseInt(environment.getProperty("binlog.datasource.id")));

        String datasourceUrl = environment.getProperty("spring.datasource.url");
        ConnectionUrlParser urlParser = ConnectionUrlParser.parseConnectionString(datasourceUrl);
        HostInfo hostInfo = urlParser.getHosts().get(0);
        binlogDataProps.setDatasourceHost(hostInfo.getHost());
        binlogDataProps.setDatasourcePort(hostInfo.getPort());

        binlogDataProps.setDatasourceUsername(environment.getProperty("spring.datasource.username"));
        binlogDataProps.setDatasourcePassword(environment.getProperty("spring.datasource.password"));
        return binlogDataProps;
    }

    @Bean(initMethod = "init", destroyMethod = "destroy")
    public BinlogDataClient binlogDataClient() {
        BinlogDataClient client = new BinlogDataClient(binlogDataProps());
        client.addSubscribers(subscriberScanner.getBinlogSubscribers());
        return client;
    }

}
