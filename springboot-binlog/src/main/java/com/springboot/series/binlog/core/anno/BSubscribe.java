package com.springboot.series.binlog.core.anno;

import com.lingshi.binlog.data.BinlogType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author shanlingshi
 * @since 2022-08-02
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface BSubscribe {

    /**
     * 订阅的表
     */
    String table();

    /**
     * 事件类型
     */
    BinlogType type();

}
