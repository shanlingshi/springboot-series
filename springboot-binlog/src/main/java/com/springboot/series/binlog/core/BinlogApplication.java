package com.springboot.series.binlog.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author shanlingshi
 * @since 2022-08-30
 */
@SpringBootApplication(scanBasePackages = "com.springboot.series.binlog")
public class BinlogApplication {

    public static void main(String[] args) {

        SpringApplication.run(BinlogApplication.class, args);
    }

}