package com.springboot.series.binlog;

import com.lingshi.binlog.data.BinlogData;
import com.lingshi.binlog.data.BinlogType;
import com.springboot.series.binlog.core.anno.BSubscribe;
import org.springframework.stereotype.Component;

@Component
public class DictUpdateSubscriber {

    @BSubscribe(table = "soch.dict", type = BinlogType.UPDATE)
    public void onSubscribe(BinlogData data) {
        System.out.println(data);
    }

}
