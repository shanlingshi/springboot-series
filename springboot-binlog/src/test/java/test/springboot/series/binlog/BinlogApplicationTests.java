package test.springboot.series.binlog;

import com.springboot.series.binlog.core.BinlogApplication;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.TimeUnit;

/**
 * 测试
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BinlogApplication.class)
public class BinlogApplicationTests {

    /**
     * 测试增删改查
     */
    @Test
    public void test() throws InterruptedException {
        System.out.println("项目启动成功");
        TimeUnit.MINUTES.sleep(30);
    }

}
