package com.springboot.sse.core.holder;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

/**
 * WebSocketSession 用于保存当前所有在线的会话信息
 *
 * @author shanlingshi
 */
public class SSESessionHolder {

    private static final Map<String, SseEmitter> SESSION_MAP = new ConcurrentHashMap<>();

    public static Set<String> getAllSession() {
        return SESSION_MAP.keySet();
    }

    public static void addSession(String uid, SseEmitter sseEmitter) {
        SESSION_MAP.put(uid, sseEmitter);
    }

    public static SseEmitter getSession(String sid) {
        return SESSION_MAP.get(sid);
    }

    public static void removeSession(String uid) {
        if (SESSION_MAP.containsKey(uid)) {
            SESSION_MAP.get(uid).complete();
            SESSION_MAP.remove(uid);
        }
    }

    public static Boolean containsSession(String sid) {
        return SESSION_MAP.containsKey(sid);
    }

}
