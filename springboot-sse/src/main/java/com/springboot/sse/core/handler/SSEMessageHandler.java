package com.springboot.sse.core.handler;

import com.springboot.sse.core.SSEMessage;
import com.springboot.sse.core.holder.SSESessionHolder;
import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

/**
 * @author shanlingshi
 */
@Component
public class SSEMessageHandler {

    private static final Logger log = LoggerFactory.getLogger(SSEMessageHandler.class);

    /**
     * 建立连接
     */
    public SseEmitter connect(String uid) {
        SseEmitter sseEmitter = SSESessionHolder.getSession(uid);

        if (!SSESessionHolder.containsSession(uid)) {
            sseEmitter = new SseEmitter(0L);
            sseEmitter.onCompletion(onCompletionCallBack(uid));
            sseEmitter.onTimeout(onTimeoutCallBack(uid));
            sseEmitter.onError(onErrorCallBack(uid));
            SSESessionHolder.addSession(uid, sseEmitter);
        }

        log.info("SSE建立连接 : {}", uid);
        return sseEmitter;
    }

    /**
     * 断开连接
     */
    public void disconnect(String uid) {
        SseEmitter sseEmitter = SSESessionHolder.getSession(uid);
        if (!ObjectUtils.isEmpty(sseEmitter)) {
            sseEmitter.complete();
            log.info("SSE断开连接 : {}", uid);
            SSESessionHolder.removeSession(uid);
        }
    }

    /**
     * 发送消息指定用户
     */
    public void sendMessage(String uid, SSEMessage message) {
        sendMessage(Collections.singletonList(uid), message);
    }

    /**
     * 发送消息指定用户
     */
    public void sendMessage(Collection<String> uidList, SSEMessage message) {
        for (String uid : uidList) {
            SseEmitter sseEmitter = SSESessionHolder.getSession(uid);
            if (sseEmitter == null) {
                continue;
            }

            doSendMessage(sseEmitter, message);
        }
    }

    /**
     * 发送消息给所有人
     */
    public void sendMessage(SSEMessage message) {

        sendMessage(SSESessionHolder.getAllSession(), message);
    }

    /**
     * 发送消息
     */
    private void doSendMessage(SseEmitter sseEmitter, SSEMessage message) {
        CompletableFuture.runAsync(() -> {
            try {
                sseEmitter.send(message);
            } catch (Exception e) {
                throw new RuntimeException("SSE推送数据异常");
            }
        });
    }

    /**
     * 通信结束
     */
    private Runnable onCompletionCallBack(String uid) {
        return () -> {
            log.info("SSE连接断开 : {}", uid);
        };
    }

    /**
     * 通讯超时
     */
    private Runnable onTimeoutCallBack(String uid) {
        return () -> {
            log.info("SSE连接超时 : {}", uid);
            SSESessionHolder.removeSession(uid);
        };
    }

    /**
     * 通讯异常
     */
    private Consumer<Throwable> onErrorCallBack(String uid) {
        return throwable -> {
            log.info("SSE连接异常 : {}", uid);
            SSESessionHolder.removeSession(uid);
        };
    }

}
