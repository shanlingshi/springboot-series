package com.springboot.sse.core;

import java.io.Serializable;

/**
 * @author shanlingshi
 * @email 171487176@qq.com
 * @since 2024-04-15
 */
public class SSEMessage implements Serializable {

    private static final long serialVersionUID = -3411790046046051744L;

    private String type;

    private String content;

    public String getType() {
        return type;
    }

    public SSEMessage setType(String type) {
        this.type = type;
        return this;
    }

    public String getContent() {
        return content;
    }

    public SSEMessage setContent(String content) {
        this.content = content;
        return this;
    }

}
